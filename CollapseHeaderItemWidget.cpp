#include "CollapseHeaderItemWidget.h"
#include <QPainter>

CollapseHeaderItemWidget::CollapseHeaderItemWidget(QWidget* parent)
	: QWidget(parent)
	, ui(new Ui::CollapseHeaderItemWidgetClass())
{
	ui->setupUi(this);
	init();
}

CollapseHeaderItemWidget::~CollapseHeaderItemWidget()
{
	delete ui;
}

void CollapseHeaderItemWidget::setExpandImage(const QImage& image)
{
	ui->expandLab->setPixmap(QPixmap::fromImage(image));
}

void CollapseHeaderItemWidget::init()
{

}

void CollapseHeaderItemWidget::paintEvent(QPaintEvent* event)
{
	QPainter painter(this);
	painter.setPen(Qt::NoPen);
	painter.setBrush(QColor(255, 255, 255, 12));
	painter.drawRoundedRect(rect(), 4, 4);
}
