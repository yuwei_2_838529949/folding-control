#include "CollapseContainerWidget.h"
#include <QtWidgets/QApplication>
#include <AiReplaceWidget.h>
#include <QFileInfo>

#ifdef Q_OS_WIN
#include <Windows.h>


void yuwDebugOutput(QtMsgType type, const QMessageLogContext& context, const QString& msg) {
	QFileInfo fileInfo(context.file);
	QString output = QString("[%1:%2] %3\r\n")
		.arg(fileInfo.fileName())
		.arg(context.line).arg(msg);
	OutputDebugString(output.toStdWString().c_str());
}
#endif

int main(int argc, char* argv[])
{
	QApplication a(argc, argv);
#ifdef Q_OS_WIN
	//设置重定向操作的函数
	qInstallMessageHandler(yuwDebugOutput);
#endif
	/*CollapseContainerWidget w;
	w.show();*/
	AiReplaceWidget w;
	w.setFixedWidth(200);
	w.show();
	return a.exec();
}
