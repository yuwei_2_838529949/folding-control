#include "HairColorItemWidget.h"
#include <QPainter>

HairColorItemWidget::HairColorItemWidget(const QString& id, QWidget* parent)
	: QWidget(parent)
	, ui(new Ui::HairColorItemWidgetClass())
	, m_id(id)
{
	ui->setupUi(this);
	resize(34, 34);
}

HairColorItemWidget::~HairColorItemWidget()
{
	delete ui;
}

void HairColorItemWidget::setColor(const QColor& color)
{
	m_color = color;
	update();
}

void HairColorItemWidget::setSelected(bool isSelected)
{
	m_isSelected = isSelected;
	update();
}

void HairColorItemWidget::paintEvent(QPaintEvent* event)
{
	QPainter painter(this);
	painter.setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
	painter.setPen(Qt::NoPen);
	painter.setBrush(m_color);
	const int padding = 3;
	QRect drawRect(padding, padding, width() - 2 * padding, height() - 2 * padding);
	painter.drawRoundedRect(drawRect, 6, 6);

	// 绘制边框
	if (m_isSelected) {
		painter.setBrush(Qt::NoBrush);
		painter.setPen(QPen(QColor(0, 151, 255)));
		painter.drawRoundedRect(1, 1, width() - 2, height() - 2, 6, 6);
	}
}

void HairColorItemWidget::mousePressEvent(QMouseEvent* event)
{
	emit sigColorSelected(m_id);
	QWidget::mousePressEvent(event);
}
