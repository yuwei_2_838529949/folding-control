#include "AiReplaceWidget.h"
#include <QStyle>
#include <QPainter>

AiReplaceWidget::AiReplaceWidget(QWidget* parent)
	: QWidget(parent)
	, ui(new Ui::AiReplaceWidgetClass())
{
	ui->setupUi(this);
	init();
}

AiReplaceWidget::~AiReplaceWidget()
{
	delete ui;
}

void AiReplaceWidget::on_femaleBtn_clicked()
{
	changeStatus(ui->maleBtn, "normal");
	changeStatus(ui->femaleBtn, "checked");
	ui->stackedWidget->setCurrentWidget(ui->femaleWgt);
}

void AiReplaceWidget::on_maleBtn_clicked()
{
	changeStatus(ui->femaleBtn, "normal");
	changeStatus(ui->maleBtn, "checked");
	ui->stackedWidget->setCurrentWidget(ui->maleWgt);
}

void AiReplaceWidget::init()
{
	setAttribute(Qt::WA_StyledBackground);
	//ui->stackedWidget->setCurrentWidget(ui->errorWgt);
	on_femaleBtn_clicked();
}

void AiReplaceWidget::changeStatus(QWidget* control, const QString& status)
{
	const QString oldStatus = control->property("status").toString();
	if (oldStatus == status)
		return;
	control->setProperty("status", status);
	control->style()->unpolish(control);
	control->style()->polish(control);
}

void AiReplaceWidget::paintEvent(QPaintEvent* event)
{
	QPainter painter(this);
	painter.setPen(Qt::NoPen);
	painter.setBrush(QColor(41, 41, 41));
	painter.drawRect(rect());
}