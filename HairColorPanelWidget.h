#pragma once

#include <QWidget>
#include "ui_HairColorPanelWidget.h"
#include <HairColorItemWidget.h>
#include <QGridLayout>


QT_BEGIN_NAMESPACE
namespace Ui { class HairColorPanelWidgetClass; };
QT_END_NAMESPACE

class HairColorPanelWidget : public QWidget
{
	Q_OBJECT

public:
	HairColorPanelWidget(QWidget* parent = nullptr);

	HairColorPanelWidget(const QString& id, QWidget* parent = nullptr);

	~HairColorPanelWidget();

	void setHairStyleId(const QString& hairStyleId);

	QString getId()const { return m_id; }

	QString getHairStyleId()const { return m_hairId; }
private:
	void createLayout();
private slots:
	void onColorSelected(const QString& id);
protected:
	virtual void paintEvent(QPaintEvent* event)override;
	virtual void showEvent(QShowEvent* event)override;
private:
	Ui::HairColorPanelWidgetClass* ui;
	QString m_id;
	QMap<QString, HairColorItemWidget*> m_hairColorMap;
	QGridLayout* m_layout = nullptr;
	QString m_hairId;
};
