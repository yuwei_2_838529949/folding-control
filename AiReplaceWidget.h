#pragma once

#include <QWidget>
#include "ui_AiReplaceWidget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class AiReplaceWidgetClass; };
QT_END_NAMESPACE

class AiReplaceWidget : public QWidget
{
	Q_OBJECT
public:
	AiReplaceWidget(QWidget* parent = nullptr);
	~AiReplaceWidget();
protected:
	void paintEvent(QPaintEvent* event)override;
private slots:
	void on_femaleBtn_clicked();
	void on_maleBtn_clicked();
private:
	void init();
	void changeStatus(QWidget* control, const QString& status);
private:
	Ui::AiReplaceWidgetClass* ui;
};
