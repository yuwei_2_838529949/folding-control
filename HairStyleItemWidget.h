#pragma once

#include <QWidget>
#include "ui_HairStyleItemWidget.h"
#include <QMovie>


QT_BEGIN_NAMESPACE
namespace Ui { class HairStyleItemWidgetClass; };
QT_END_NAMESPACE

class HairStyleItemWidget : public QWidget
{
	Q_OBJECT

public:
	HairStyleItemWidget(QWidget* parent = nullptr);
	HairStyleItemWidget(const QString& url, const QString& id, QWidget* parent = nullptr);
	~HairStyleItemWidget();
public:
	void setTitle(const QString& title);
	void setSelected(bool isSelected);
	QString getId()const { return m_id; }
signals:
	void sigSelected(const QString& id);
private:
	void createMovie();
protected:
	virtual void paintEvent(QPaintEvent* event)override;
	virtual void mousePressEvent(QMouseEvent* event)override;
	virtual void enterEvent(QEnterEvent* event)override;
	virtual void leaveEvent(QEvent* event)override;
private:
	Ui::HairStyleItemWidgetClass* ui;
	QString m_url;
	QImage m_image;
	QString m_id;
	bool m_isSelected = false;
	bool m_isHover = false;
	QMovie* m_movie = nullptr;
	enum HairStyleStatus
	{
		Loading_Enum,
		Fail_Enum,
		Successed_Enum
	};
	HairStyleStatus m_status = HairStyleStatus::Successed_Enum;
};
