#include "HairStyleItemWidget.h"
#include <QPainter>
#include <QFile>
#include <QPainterPath>
#include <QStyle>


QImage RoundImage(const QImage& image, int radius) {
	QImage dest(image.size(), QImage::Format_RGBA8888);
	dest.fill(Qt::transparent);
	QPainter painter(&dest);
	// 抗锯齿 + 平滑边缘处理
	painter.setRenderHints(QPainter::Antialiasing, true);
	painter.setRenderHints(QPainter::SmoothPixmapTransform, true);
	// 裁剪为圆角
	QPainterPath path;
	path.addRoundedRect(dest.rect(), radius, radius);
	painter.setClipPath(path);
	painter.drawImage(dest.rect(), image);
	return dest;
}

QPixmap RoundPixmap(const QPixmap& src, int radius) {
	if (src.isNull()) {
		return QPixmap();
	}
	QPixmap dest(src.size());
	dest.fill(Qt::transparent);
	QPainter painter(&dest);
	// 抗锯齿 + 平滑边缘处理
	painter.setRenderHints(QPainter::Antialiasing, true);
	painter.setRenderHints(QPainter::SmoothPixmapTransform, true);
	// 裁剪为圆角
	QPainterPath path;
	path.addRoundedRect(dest.rect(), radius, radius);
	painter.setClipPath(path);
	painter.drawPixmap(dest.rect(), src);
	return dest;
}


HairStyleItemWidget::HairStyleItemWidget(QWidget* parent)
	:HairStyleItemWidget(":/images/zz.jpg", "0", parent)
{

}

HairStyleItemWidget::HairStyleItemWidget(const QString& url, const QString& id, QWidget* parent)
	: QWidget(parent)
	, ui(new Ui::HairStyleItemWidgetClass())
	, m_id(id)
	, m_url(url)
{
	ui->setupUi(this);
	setMouseTracking(true);
	createMovie();
	m_image = QImage(url);
	ui->label->adjustSize();
	setTitle("Long Straight");
	setFixedHeight(94);
}

HairStyleItemWidget::~HairStyleItemWidget()
{
	delete ui;
}

void HairStyleItemWidget::setTitle(const QString& title)
{
	QString s = ui->label->fontMetrics().elidedText(title, Qt::ElideRight, ui->label->width());
	ui->label->setText(s);
}

void HairStyleItemWidget::setSelected(bool isSelected)
{
	m_isSelected = isSelected;
	update();
}

void HairStyleItemWidget::createMovie()
{
	m_movie = new QMovie(":/images/Screen Scratching.gif");
	m_movie->setParent(this);
	m_movie->setCacheMode(QMovie::CacheNone);
	connect(m_movie, &QMovie::frameChanged, this, [=] {
		update();
		});
	m_movie->start();
}

void HairStyleItemWidget::paintEvent(QPaintEvent* event)
{
	QPainter painter(this);
	painter.setRenderHints(QPainter::SmoothPixmapTransform | QPainter::Antialiasing);
	painter.setPen(Qt::NoPen);
	int padding = 8;

	if (HairStyleStatus::Loading_Enum == m_status) {
		QPixmap gifPix = m_movie->currentPixmap();
		QPixmap scaledPixmap = gifPix.scaledToWidth(width() - padding, Qt::SmoothTransformation);
		const QPixmap roundPix = RoundPixmap(scaledPixmap, 4);
		painter.drawPixmap(QPoint(padding / 2, padding / 2), roundPix, QRect(0, 0, roundPix.width(), 80));
	}
	else if (HairStyleStatus::Fail_Enum == m_status) {
		QImage image;
		if (m_isHover) {
			image = QImage(":/images/ic_refresh_hover.png");
		}
		else {
			image = QImage(":/images/ic_refresh_normal.png");
		}
		painter.drawImage(QPoint((width() - image.width()) / 2, 40), image);
	}
	else if (HairStyleStatus::Successed_Enum == m_status) {
		// 绘制图片
		const QImage scaleImage = m_image.scaledToWidth(width() - padding, Qt::SmoothTransformation);

		//const QImage scaleImage = m_image.scaled(size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);

		const QImage roundImage = RoundImage(scaleImage, 4);
		painter.drawImage(padding / 2, padding / 2, roundImage);

		// 绘制外边框
		const QSize imageSize = scaleImage.size() + QSize(padding / 2, padding / 2);
		if (m_isSelected) {
			painter.setPen(QPen(QColor(44, 125, 250), 1));
			painter.drawRoundedRect(padding / 4, padding / 4, imageSize.width(), imageSize.height(), 4, 4);
		}
		else if (m_isHover) {
			// 图片添加30%黑色透明度
			painter.setBrush(QColor(0, 0, 0, 255 * 0.3));
			painter.drawRoundedRect(padding / 4, padding / 4, imageSize.width(), imageSize.height(), 4, 4);
		}
	}

	painter.setPen(Qt::red);
	painter.drawRoundedRect(rect(), 6, 6);
}

void HairStyleItemWidget::mousePressEvent(QMouseEvent* event)
{
	if (m_status == HairStyleStatus::Successed_Enum) {
		emit sigSelected(m_id);
	}
	else if (m_status == HairStyleStatus::Fail_Enum) {
		// 重新下载图片
	}
	QWidget::mousePressEvent(event);
}

void HairStyleItemWidget::enterEvent(QEnterEvent* event)
{
	m_isHover = true;
	ui->label->setProperty("status", "hover");
	ui->label->style()->unpolish(ui->label);
	ui->label->style()->polish(ui->label);
	// 需要设置焦点，否则hover不生效
	setFocus();
	QWidget::enterEvent(event);
}

void HairStyleItemWidget::leaveEvent(QEvent* event)
{
	m_isHover = false;
	ui->label->setProperty("status", "normal");
	ui->label->style()->unpolish(ui->label);
	ui->label->style()->polish(ui->label);
	clearFocus();
	QWidget::leaveEvent(event);
}
