#pragma once

#include <QWidget>
#include "ui_AiReplacePanelWidget.h"
#include <QPushButton>
#include <HairStyleItemWidget.h>
#include <HairColorPanelWidget.h>
#include <QGridLayout>

QT_BEGIN_NAMESPACE
namespace Ui { class AiReplacePanelWidgetClass; };
QT_END_NAMESPACE

class AiReplacePanelWidget : public QWidget
{
	Q_OBJECT

public:
	AiReplacePanelWidget(QWidget* parent = nullptr);
	~AiReplacePanelWidget();
private:
	void init();
	void createLayout();
protected:
	virtual void showEvent(QShowEvent* event)override;
private slots:
	void onHairStyleItemSelected(const QString& id);
private:
	Ui::AiReplacePanelWidgetClass* ui;
	QMap<QString, HairStyleItemWidget*> m_HairStyleMap;
	QMap<QString, HairColorPanelWidget*> m_HairColorMap;
	QGridLayout* m_layout = nullptr;
};
