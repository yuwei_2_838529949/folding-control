#include "CollapseContainerWidget.h"
#include <CollapseItemWidget.h>
#include <AiReplaceWidget.h>

CollapseContainerWidget::CollapseContainerWidget(QWidget* parent)
	: QWidget(parent)
	, ui(new Ui::CollapseContainerWidgetClass())
{
	ui->setupUi(this);
	setAttribute(Qt::WA_StyledBackground);
	// ui->scrollAreaWidgetContents的显示区域是内部控件的最小尺寸
	QVBoxLayout* pLayout = new QVBoxLayout(ui->scrollAreaWidgetContents);
	// ui->scrollAreaWidgetContents外边距
	pLayout->setContentsMargins(0, 0, 0, 0);
	// item之间的间隔
	pLayout->setSpacing(6);

	for (int i(0); i < 2; ++i) {
		CollapseItemWidget* item = new CollapseItemWidget();
		AiReplaceWidget* aiReplaceWgt = new AiReplaceWidget();
		item->setContentWidget(aiReplaceWgt);
		pLayout->addWidget(item);

		connect(item, &CollapseItemWidget::sigSelected, [=] {});
	}
	QSpacerItem* spacerItem = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding);
	pLayout->addItem(spacerItem);


	// test
	AiReplaceWidget* aiReplaceWgt = new AiReplaceWidget();
	aiReplaceWgt->show();
}

CollapseContainerWidget::~CollapseContainerWidget()
{
	delete ui;
}

