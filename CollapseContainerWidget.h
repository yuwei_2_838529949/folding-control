#pragma once

#include <QtWidgets/QWidget>
#include "ui_CollapseContainerWidget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class CollapseContainerWidgetClass; };
QT_END_NAMESPACE


class CollapseContainerWidget : public QWidget
{
	Q_OBJECT
public:
	CollapseContainerWidget(QWidget* parent = nullptr);

	~CollapseContainerWidget();
private:
	Ui::CollapseContainerWidgetClass* ui;
};
