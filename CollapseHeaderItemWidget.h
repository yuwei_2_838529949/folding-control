#pragma once

#include <QWidget>
#include "ui_CollapseHeaderItemWidget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class CollapseHeaderItemWidgetClass; };
QT_END_NAMESPACE

class CollapseHeaderItemWidget : public QWidget
{
	Q_OBJECT

public:
	CollapseHeaderItemWidget(QWidget* parent = nullptr);

	~CollapseHeaderItemWidget();

	void setExpandImage(const QImage& image);
private:
	void init();
protected:
	void paintEvent(QPaintEvent* event)override;
private:
	Ui::CollapseHeaderItemWidgetClass* ui;
};
