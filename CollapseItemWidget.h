#pragma once

#include <QWidget>
#include "ui_CollapseItemWidget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class CollapseItemWidgetClass; };
QT_END_NAMESPACE

class CollapseItemWidget : public QWidget
{
	Q_OBJECT

public:
	CollapseItemWidget(QWidget* parent = nullptr);

	~CollapseItemWidget();

	void setContentWidget(QWidget* contentWgt);
signals:
	void sigSelected(bool isSelected);
protected:
	virtual bool eventFilter(QObject* watched, QEvent* ev)override;
	virtual void resizeEvent(QResizeEvent* event)override;
private:
	void refreshMinHeight();
	void setExpand(bool isExpand);
private:
	Ui::CollapseItemWidgetClass* ui;
	bool m_isSelected = false;
	QWidget* m_contentWgt = nullptr;
};
