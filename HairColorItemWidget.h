#pragma once

#include <QWidget>
#include "ui_HairColorItemWidget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class HairColorItemWidgetClass; };
QT_END_NAMESPACE

class HairColorItemWidget : public QWidget
{
	Q_OBJECT

public:
	HairColorItemWidget(const QString& id, QWidget* parent = nullptr);

	~HairColorItemWidget();

	void setColor(const QColor& color);

	void setSelected(bool isSelected);

	QString getId()const { return m_id; }
signals:
	void sigColorSelected(const QString& id);
protected:
	virtual void paintEvent(QPaintEvent* event)override;
	virtual void mousePressEvent(QMouseEvent* event)override;
private:
	Ui::HairColorItemWidgetClass* ui;
	QColor m_color = QColor(std::rand() % 256, std::rand() % 256, std::rand() % 256);
	bool m_isSelected = false;
	QString m_id;
};
